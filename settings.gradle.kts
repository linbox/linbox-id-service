rootProject.name = "id-service"
dependencyResolutionManagement {
    repositories {
        mavenLocal()
        maven { url = uri("https://maven.aliyun.com/repository/public") }
        mavenCentral()
        maven { url = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/") }
    }
    versionCatalogs {
        create("linboxLibs") {
            if (findProject(":linbox-dependency-management") != null) {
                from(files("../linbox-dependency-management/linbox-versions.toml"))
            } else {
                from("tech.linbox:dependency-management:3.0.0-SNAPSHOT")
            }
        }
    }
}