import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

@Suppress(
    "DSL_SCOPE_VIOLATION",
    "MISSING_DEPENDENCY_CLASS",
    "UNRESOLVED_REFERENCE_WRONG_RECEIVER",
    "FUNCTION_CALL_EXPECTED"
)

plugins {
    alias(linboxLibs.plugins.spring.boot)
    alias(linboxLibs.plugins.spring.dependency.management)
    alias(linboxLibs.plugins.kotlin.spring)
    alias(linboxLibs.plugins.kotlin.jvm)
    alias(linboxLibs.plugins.kotlin.kapt)

    `java-library`
    `maven-publish`
    signing
}

group = "tech.linbox"
version = "3.0.0-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_17

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

configurations.all {
    resolutionStrategy {
        cacheChangingModulesFor(0, "seconds")
    }
}

dependencies {
    kapt("org.springframework.boot:spring-boot-configuration-processor")
    implementation("org.springframework.boot:spring-boot-starter")

    // kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation(linboxLibs.kotlin.coroutines.core)

    if (findProject(":linbox-common-spring") != null) {
        implementation(project(":linbox-common-spring"))
    } else {
        implementation("tech.linbox:linbox-common-spring:1.0.0-SNAPSHOT")
    }

    if (findProject(":linbox-id-api") != null) {
        implementation(project(":linbox-id-api"))
    } else {
        implementation("tech.linbox:id-api:3.0.0-SNAPSHOT")
    }

    if (findProject(":linbox-uid-generator-spring-boot-starter") != null) {
        implementation(project(":linbox-uid-generator-spring-boot-starter"))
    } else {
        implementation("tech.linbox:uid-generator-spring-boot-starter:1.0.0-SNAPSHOT")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "17"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

sourceSets.main {
    java.srcDirs("src/main/kotlin")
}

tasks.jar {
    enabled = true
}

tasks.bootJar {
    enabled = false
}

java {
    withJavadocJar()
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("mavenJava") {
            from(components["java"])
            pom {
                name.set("linbox id service")
                description.set("Project for produce uid")
                url.set("https://gitee.com/linbox/linbox-id-service")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("Linbox Company")
                        name.set("Linbox Company")
                        email.set("company@bhry.tech")
                    }
                }
                scm {
                    connection.set("scm:git:git@gitee.com:linbox/linbox-id-service.git")
                    developerConnection.set("scm:git:git@gitee.com:linbox/linbox-id-service.git")
                    url.set("https://gitee.com/linbox/linbox-id-service")
                }
            }
        }
    }
}

signing {
    setRequired({
        gradle.taskGraph.hasTask("publish")
    })
    sign(publishing.publications["mavenJava"])
}


