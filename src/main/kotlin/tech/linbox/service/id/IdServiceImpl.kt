package tech.linbox.service.id

import tech.linbox.uid.UidGenerator

class IdServiceImpl(private val defaultUidGenerator: UidGenerator) : IdService {
    override fun getId(): String {
        return defaultUidGenerator.uid.toString()
    }
}