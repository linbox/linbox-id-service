package tech.linbox.service.id.config

import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.annotation.Bean
import tech.linbox.service.id.IdService
import tech.linbox.service.id.IdServiceImpl
import tech.linbox.uid.impl.DefaultUidGenerator

@AutoConfiguration
@ConditionalOnClass(name = ["tech.linbox.service.id.IdService"])
class IdServiceAutoConfig {

    @Bean
    @ConditionalOnMissingBean
    fun idService(defaultUidGenerator: DefaultUidGenerator): IdService {
        return IdServiceImpl(defaultUidGenerator)
    }

}